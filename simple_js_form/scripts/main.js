console.log('Page Load Happened')

var submitButton = document.getElementById('submit-button')
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log("Entered get Form Info!")
    // get text from recipe/ingredients/instructions
    var recipe_title = document.getElementById('recipe-title').value;
    var recipe_ingredients = document.getElementById('recipe-ingredients').value;
    var recipe_instructions = document.getElementById('recipe-instructions').value;
    console.log('Name:' + recipe_title + ' ingredients: ' + recipe_ingredients + ' instructions: ' + recipe_instructions);
    

    // make dictionary
    recipe_dict = {};
    recipe_dict['title'] = recipe_title;
    recipe_dict['ingredients'] = recipe_ingredients;
    recipe_dict['instructions'] = recipe_instructions;
    console.log(recipe_dict);

    displayRecipe(recipe_dict);
}

function displayRecipe(recipe_dict){
    console.log('entered displayRecipe!');
    
    // get fields from story and display in label.
    var recipe_name = document.getElementById('recipe-title-display');
    recipe_name.innerHTML = recipe_dict['title'];

    var recipe_ingr = document.getElementById('recipe-ingredients-display');
    recipe_ingr.innerHTML = recipe_dict['ingredients'];

    var recipe_instr = document.getElementById('recipe-instructions-display');
    recipe_instr.innerHTML = recipe_dict['instructions'];

}
