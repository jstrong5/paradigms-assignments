import unittest
import requests
import json

class TestReset(unittest.TestCase):

		SITE_URL = 'http://localhost:51054' # replace with your port id
		print("Testing for server: " + SITE_URL)
		RESET_URL = SITE_URL + '/reset/'

		def test_put_reset_index(self):
				m = {}
				m = json.dumps(m)
				r = requests.put(self.RESET_URL, data=m)
				resp_json = json.loads(r.content.decode())

				r = requests.get(self.SITE_URL + "/movies/54")
				resp_json2 = json.loads(r.content.decode())

				self.assertEqual('success', resp_json['result'])
				self.assertEqual(resp_json2['title'], 'Big Green, The (1995)')

		def test_put_reset_key(self):
				m = {}
				m = json.dumps(m)
				r = requests.put(self.RESET_URL + "/54", data=m)
				resp_json = json.loads(r.content.decode())
				
				r = requests.get(self.SITE_URL + "/movies/54")
				resp_json2 = json.loads(r.content.decode())

				self.assertEqual('success', resp_json['result'])
				self.assertEqual(resp_json2['title'], 'Big Green, The (1995)')

if __name__ == "__main__":
		unittest.main()
