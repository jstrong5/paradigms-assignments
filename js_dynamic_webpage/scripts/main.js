console.log("in main.js")

var submitButton = document.getElementById('submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo() {
  console.log('enter getFormInfo')

  var name = document.getElementById("name-field").value;
  console.log('Name: ' + name);
  genderApiCall(name);
}

function genderApiCall(name) {
  console.log('entered make nw call' + name);
    // set up url
  var xhr = new XMLHttpRequest(); // 1 - creating request object
  var url = "https://api.genderize.io/?name=" + name;
  xhr.open("GET", url, true); // 2 - associates request attributes with xhr

  // set up onload
  xhr.onload = function(e) { // triggered when response is received
    // must be written before send
    console.log(xhr.responseText);

    var response_json = JSON.parse(xhr.responseText);
    console.log("gender: " + response_json['gender']);
    var gender = response_json['gender'];

    getYodaTalk(gender);
  }

  // set up onerror
  xhr.onerror = function(e) { // triggered when error response is received and must be before send
      console.error(xhr.statusText);
  }

  // actually make the network call
  xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function getYodaTalk(gender) {
  console.log('entered getYodaTalk');
  // set up url
  var xhr = new XMLHttpRequest(); // 1 - creating request object
  var url = "https://api.funtranslations.com/translate/yoda.json?text=i+am+a+" + gender;
  xhr.open("GET", url, true) // 2 - associates request attributes with xhr

  // set up onload
  xhr.onload = function(e) { // triggered when response is received
      // must be written before send
      console.log("Yoda Response: " + xhr.responseText);
      // do something
      updateHtml(xhr.responseText);
  }

  // set up onerror
  xhr.onerror = function(e) { // triggered when error response is received and must be before send
      console.error(xhr.statusText);
  }

  // actually make the network call
  xhr.send(null) // last step - this actually makes the request
} // end of make nw call

function updateHtml(response_text) {
  var response_json = JSON.parse(response_text);

  // dynamically adding label
  label_item = document.createElement("label"); // "label" is a classname
  label_item.setAttribute("id", "dynamic-label" ); // setAttribute(property_name, value)

  if ('contents' in response_json == true) {
    var item_text = document.createTextNode(response_json['contents']['translated']); // creating new text
    label_item.appendChild(item_text); // adding something to button with appendChild()
  }
  else {
    var item_text = document.createTextNode("Yoda Can't Talk Right Now!!!!!! Try Again Later"); // creating new text
    label_item.appendChild(item_text);
  }

  // adding label as sibling to paragraphs
  var response_div = document.getElementById("response-div");
  response_div.appendChild(label_item);

}
