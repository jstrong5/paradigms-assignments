console.log("Page Loaded");

var submitButton = document.getElementById('submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo() {
  console.log("Getting Form Information");

  form_dict = {}

  // get text inputs
  var serverName = document.getElementById('serverNameID').value;
  var portNum = document.getElementById('portNumberID').value;
  var keyNum = document.getElementById('keyID').value;
  var messageBody = document.getElementById('messageBodyID').value;
  console.log("Server: " + serverName + "\nPort Number: " + portNum + "\nKey: " + keyNum + "\nMessage Body: " + messageBody)

  form_dict['server'] = serverName;
  form_dict['port'] = portNum;
  form_dict['key'] = keyNum;
  form_dict['messageBody'] = messageBody;
  form_dict['check'] = []

  // get checkbox inputs
  if (document.getElementById('useKeyID').checked){
    form_dict['check'].push('key');
  }
  if (document.getElementById('useMessageBodyID').checked) {
    form_dict['check'].push('message');
  }

  // get radio button inputs
  if (document.getElementById('GETID').checked) {
    form_dict['request'] = 'get';
  }
  else if (document.getElementById('PUTID').checked) {
    form_dict['request'] = 'put';
  }
  else if (document.getElementById('POSTID').checked) {
    form_dict['request'] = 'post';
  }
  else if (document.getElementById('DELETEID').checked) {
    form_dict['request'] = 'delete';
  }

  serverRequest(form_dict);
}

function serverRequest(form_dict) {
  console.log("in serverRequest");

  var xhr = new XMLHttpRequest();
  var url = "http://" + form_dict['server'] + ":" + form_dict['port'] + "/movies/";

  for (var i = 0; i < form_dict['check'].length; i++) {
    if (form_dict['check'][i] == 'key') {
      url = url + form_dict['key'];
    }
  }
  console.log("URL: " + url);
  console.log("Request Type: " + form_dict['request'].toUpperCase());
  xhr.open(form_dict['request'].toUpperCase(), url, true);

  xhr.onload = function(e) {
    display(xhr.responseText);
  }

  xhr.onerror = function(e) {
    console.error(xhr.statusText);
  }

  var useMessageBody = 0;

  for (var i = 0; i < form_dict['check'].length; i++) {
    if (form_dict['check'][i] == 'message') {
      useMessageBody = 1;
    }
  }
  if (useMessageBody == 1) {
    xhr.send(form_dict['messageBody']);
  }
  else {
    xhr.send(null);
  }
}

function display(response) {
  var displayArea = document.getElementById('display');
  displayArea.innerHTML = response;
}
